package by.home.chevrolet.statement;

public class PrepareStatementConstant {
    public static final String SQL_FIND_ALL_CAR = "SELECT * FROM car;";
    public static final String SQL_CREATE_NEW_MANAGER = "INSERT INTO manager(mail, managerName, managerSurname, managerPatronymic, managerTelephone, managerLogin, managerPassword) VALUES (?, ?, ?, ?, ?, ?, ?);";
    public static final String SQL_GET_LOGIN_FROM_FORM = "SELECT * FROM manager WHERE managerLogin =?;";
    public static final String SQL_DECRYPT_PASS = "SELECT managerPassword FROM manager WHERE managerLogin=?";
    public static final String GET_MANAGER_ID_BY_LOGIN = "SELECT idManager FROM manager WHERE managerLogin =?;";
    public static final String GET_ALL_COLLEAGUES = "SELECT * FROM manager WHERE idManager!=?;";
    public static final String GET_ALL_CLIENTS = "SELECT * FROM client;";
    public static final String GET_CLIENT_TYPE = "SELECT * FROM clienttype;";
    public static final String GET_CAR_ID_BY_VIN = "SELECT idCar FROM car WHERE VIN =?;";
    public static final String GET_CLIENT_TYPE_DESCRIPTION = "SELECT client_Description FROM clienttype WHERE idClientType =?;";
    public static final String GET_ONE_CONTRACT_TO_RESERVE = "SELECT * FROM contract WHERE idCar =?;";
    public static final String GET_ALL_MY_CONTRACTS = "SELECT contract.id, idCar, idClient, manager.idManager, contractDate," +
            " paymentDate, shipmentDate FROM contract, manager WHERE contract.idManager=manager.idManager and manager.idManager=?;";
    public static final String GET_CLIENT_INFORMATION = "SELECT clientName, clientSurname, telephone, clientMail FROM client, contract WHERE contract.idClient = client.idClient AND contract.id = ?;";
    public static final String GET_CAR_INFORMATION = "SELECT VIN, carModel FROM car, contract WHERE car.idCar = contract.idCar AND contract.id = ?;";
    public static final String UPDATE_CAR_STATUS = "UPDATE car SET status =? WHERE car.idCar =?;";
    public static final String GET_CLIENT_ID_BY_PHONE = "SELECT idClient FROM client WHERE client.telephone =?;";
    public static final String INSERT_NEW_CONTRACT = "INSERT INTO contract (idCar, idClient, idManager, contractDate, paymentDate) VALUES (?, ?, ?, ?, ?);";
    public static final String ADD_NEW_CAR = "INSERT IGNORE INTO car(VIN, carModel, Colour, carPrice, availability, recyclingCollection) VALUES (?,?,?,?,?,?);";
    public static final String UPDATE_CAR_INFO = "UPDATE IGNORE car SET carPrice =?, availability =?, recyclingCollection =? WHERE VIN =?;";
    public static final String DELETE_CAR_IF_NECESSARY = "DELETE FROM car WHERE VIN =?;";
    public static final String DELETE_CONTRACT_FROM_DB_BY_CAR = "DELETE FROM contract WHERE idCar =?;";
    public static final String ADD_NEW_CLIENT ="INSERT IGNORE INTO client(clientMail, telephone, notes, clientName, clientSurname, clientPatronymic, idClientType) VALUES (?, ?, ?, ?, ?, ?, ?);";
    public static final String UPDATE_CLIENT_INFO = "UPDATE IGNORE client SET clientMail =?, telephone=?, notes=?, clientName=?, clientSurname=?, clientPatronymic=? WHERE idClient=?";
    public static final String DELETE_CONTRACT_FROM_DB_BY_CLIENT = "DELETE FROM contract WHERE idClient =?;";
    public static final String DELETE_CLIENT_IF_NECESSARY = "DELETE FROM client WHERE idClient =?;";
    public static final String DELETE_CONTRACT_FROM_SALES = "DELETE FROM contract WHERE id =?;";
    public static final String UPDATE_CAR_STATUS_FROM_SALES ="UPDATE car SET status =? WHERE VIN =?;";
    public static final String UPDATE_CONTRACT_STATUS = "UPDATE IGNORE contract SET contractDate =?, paymentDate =?, shipmentDate =? WHERE id =?;";
}
