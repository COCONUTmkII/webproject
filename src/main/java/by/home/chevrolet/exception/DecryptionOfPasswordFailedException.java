package by.home.chevrolet.exception;

import org.apache.log4j.Logger;

public class DecryptionOfPasswordFailedException extends Exception {
    private static final Logger logger = Logger.getLogger(DecryptionOfPasswordFailedException.class);
    /**
     * This exception can be thrown in case when password decryption is faled.
     */
    public DecryptionOfPasswordFailedException(){
        logger.error("Decryption of password is failed");
    }
}
