package by.home.chevrolet.exception;

import org.apache.log4j.Logger;

import java.io.IOException;

public class BinFileNotFoundException extends IOException {
    private static final Logger logger = Logger.getLogger(BinFileNotFoundException.class);
    /**
     * This is an exception that can be thrown in RSA algorithm if encrypted BIn file is not found
     */
    public BinFileNotFoundException(){
        logger.error("Bin encrypted file was not found");
    }
}
