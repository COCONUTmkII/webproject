package by.home.chevrolet.test;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

public class AESAlgorithmTest {
    private Map<String, Object> keys = null;
    @BeforeTest
    public void testWriteRSAKeys() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(512);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        keys =new HashMap<>();
        keys.put("private", privateKey);
        keys.put("public", publicKey);
        Assert.assertNotNull(keys);
    }

    @Test
    public void testGetHashEncryptedFile() {
        PrivateKey privateKey = (PrivateKey)keys.get("private");
        Assert.assertNotNull(privateKey);
    }

    @Test
    public void testDecryptMessage() {
        PublicKey publicKey = (PublicKey) keys.get("public");
        Assert.assertNotNull(publicKey);
    }
}