package by.home.chevrolet.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CSSDefendenceTest {

    @Test
    public void testDefendeParams() {
        String testString = "<hello>";
        testString= testString.replaceAll("<", " ");
        testString = testString.replaceAll(">", " ");
        Assert.assertEquals(testString, " hello ");
    }
}