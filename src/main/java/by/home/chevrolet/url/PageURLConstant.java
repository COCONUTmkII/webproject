package by.home.chevrolet.url;

public class PageURLConstant {
    public final static String INDEX_PAGE = "/pages/index.jsp";
    public final static String ERROR_PAGE = "/pages/error.jsp";
    public final static String MAIN_PAGE = "/pages/main.jsp";
    public final static String REGISTRATION_PAGE = "/pages/registration.jsp";
    public final static String VEHICLE_PAGE = "/pages/vehicles.jsp";
    public final static String OWNER_PAGE = "/pages/owners.jsp";
    public final static String SALE_PAGE = "/pages/sales.jsp";
    public final static String COLLEAGUE_PAGE = "/pages/colleagues.jsp";
}
