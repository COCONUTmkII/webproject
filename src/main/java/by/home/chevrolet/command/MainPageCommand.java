package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class MainPageCommand implements ActionCommand {
    /**
     * This is a simple command to return manager to main page
     * @param request Servlet request
     * @return Returns to main page each manager.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.MAIN_PAGE;
        return page;
    }
}
