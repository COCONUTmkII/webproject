package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOClient;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

public class ClientUpdateCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(ClientUpdateCommand.class);
    /**
     * This command is used to update some client information if there was mistake made by manager or
     * if client parameters was changed, e.g. telephone.
     * @param request Servlet request
     * @return Returns a owners page if command is successful. If manager is not authorized - to index.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        }
        String clientMail = request.getParameter("mail-upd");
        String telephone = request.getParameter("phone-upd");
        String notes = request.getParameter("notes-upd");
        String clientName = request.getParameter("name-upd");
        String clientSurname = request.getParameter("surname-upd");
        String clientPatronymic = request.getParameter("patronymic-upd");
        int clientId = Integer.valueOf(request.getParameter("id-upd"));
        DAOClient daoClient = new DAOClient();
        try {
            daoClient.updateClient(clientId, clientMail, telephone, notes, clientName, clientSurname, clientPatronymic);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("Client" + clientId + "was updated by this manager" + idManager);
        page = PageURLConstant.OWNER_PAGE;
        return page;
    }
}
