package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOManager;
import by.home.chevrolet.entity.Manager;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class ColleagueCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(ColleagueCommand.class);
    /**
     * This command is used to represent each manager colleagues.
     * @param request Servlet reuqest
     * @return Returns a colleagues page if command is successful. If manager is not authorized - to index.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object id = session.getAttribute("idManager");
        if (id == null){
            logger.error("Manager is not aughorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
        } else {
            DAOManager daoManager = new DAOManager();
            List<Manager> colleagues = new ArrayList<Manager>();
            colleagues = daoManager.showColleagues((int)id);
            request.setAttribute("listColleagues", colleagues);
            page = PageURLConstant.COLLEAGUE_PAGE;
            return page;
        }
        return page;
    }
}
