package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOClient;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

public class ClientAddCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(ClientAddCommand.class);
    /**
     * This command is used to create new client in database.
     * @param request Servlet request
     * @return Returns a owners page if command is successful. If manager is not authorized - to index.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null) {
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        } else {
            String clientMail = request.getParameter("client-mail-add");
            String telephone = request.getParameter("client-phone-add");
            String clientName = request.getParameter("client-name-add");
            String clientSurname = request.getParameter("client-surname-add");
            String clientPatronymic = request.getParameter("client-patronymic-add");
            String notes = request.getParameter("notes-add");
            Integer clientType = Integer.valueOf(request.getParameter("client-type"));
            DAOClient daoClient = new DAOClient();
            try {
                daoClient.addNewClient(clientMail, telephone, clientName,
                        clientSurname, clientPatronymic, notes, clientType);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.info("Manager" + idManager + " added a new client in database");
            page = PageURLConstant.OWNER_PAGE;
            return page;
        }
    }
}
