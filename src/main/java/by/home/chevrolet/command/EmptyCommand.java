package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(EmptyCommand.class);
    /**
     * Thus is empty command. This command used to return an error page if user's command is unknown
     * or not supported.
     * @param request Servlet request
     * @return Returns an error page
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        logger.info("Error page command was detected. Redirect to error page");
        String page = PageURLConstant.ERROR_PAGE;
        return page;
    }
}
