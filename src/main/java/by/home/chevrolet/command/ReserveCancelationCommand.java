package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOContract;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ReserveCancelationCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(ReserveCancelationCommand.class);
    /**
     * This command is used to cancel a reservation of a car if manager wants to do this.
     * Usually reserve is canceled if client changed his mind to buy a car.
     * Also changes car status to *free* if reservation is canceled.
     * @param request Servlet request
     * @return Returns string url of vehicle page or index page if manager is not authorized
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        } else {
            String VIN = request.getParameter("VIN");
            int contractID = Integer.valueOf(request.getParameter("ID"));
            DAOContract daoContract = new DAOContract();
            daoContract.deleteContractFromSales(VIN, contractID);
            page = PageURLConstant.SALE_PAGE;
            logger.info("Manager" + idManager + "canceled reservation of car with" + VIN + "vin");
            return page;
        }
    }
}
