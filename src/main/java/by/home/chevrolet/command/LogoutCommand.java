package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(LogoutCommand.class);
    /**
     * This is a command to logout from the system.
     * @param request Servlet request
     * @return Returns index page and invalidate manager session.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.INDEX_PAGE;
        Object idManager = request.getAttribute("idManager");
        request.setAttribute("idManager", null);
        request.getSession().invalidate();
        logger.info("Manager with" + idManager + "id was logout");
        return page;
    }
}
