package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOCar;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CarUpdateCommand implements ActionCommand{
    private final static Logger logger = Logger.getLogger(CarUpdateCommand.class);
    /**
     * This method is used to update some information of car if there was some mistake made my manager or if
     * some car parameters are changed, e.g. price.
     * @param request Servlet request
     * @return Returns a vehicle page if command is successful. If manager is not authorized - to index.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        }
        int carPrice = Integer.valueOf(request.getParameter("price-upd"));
        String VIN = request.getParameter("VIN-upd");
        boolean availability = request.getParameter("availability-upd") != null;
        boolean recyclingCollection = request.getParameter("data-sheet-upd") != null;
        DAOCar daoCar = new DAOCar();
        daoCar.updateInformationInCar(carPrice, VIN, availability, recyclingCollection);
        page = PageURLConstant.VEHICLE_PAGE;
        logger.info("Car with" + VIN + "vin-number was updated by" + idManager + "manager");
        return page;
    }
}
