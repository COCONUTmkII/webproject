package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LanguageCommand implements ActionCommand{
    private final static Logger logger = Logger.getLogger(LanguageCommand.class);

    /**
     * This is a command to change language and country if necessary. Languages are english and russian.
     * @param request Servlet request
     * @return Returns to main page if language is changed. Language will be saved untill user will not change it.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        String language = request.getParameter("locale").substring(0, request.getParameter("locale").indexOf("_"));
        String country = request.getParameter("locale").substring(request.getParameter("locale").indexOf("_")+1);

        session.setAttribute("localeLanguage", language);
        session.setAttribute("localeCountry", country);
        page = PageURLConstant.MAIN_PAGE;
        logger.info("language was changed to" + language);
        return page;
    }
}
