package by.home.chevrolet.command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ActionCommandFactory {
    private final static Logger logger = Logger.getLogger(ActionCommandFactory.class);
    /**
     *
     * @param request
     * @return returns ActionCommand to perform a command
     */
    public ActionCommand defineCommandFromClient(HttpServletRequest request){
        ActionCommand command = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()){
            logger.info("Command is null");
            return command;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            command = currentEnum.getCommand();
            logger.info("Command is " + action);

        } catch (IllegalArgumentException e){
            logger.error(e.getCause());
        }
        return command;
    }
}
