package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOManager;
import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.encryption.Cryptography;
import by.home.chevrolet.entity.Manager;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static by.home.chevrolet.encryption.CSSDefendence.defendeParams;
import static by.home.chevrolet.statement.PrepareStatementConstant.SQL_GET_LOGIN_FROM_FORM;

public class RegistrationCommand implements ActionCommand{
    private final static Logger logger = Logger.getLogger(RegistrationCommand.class);
    /**
     * This is registration command to register new manager if it's necessary. Login's of managers
     * are unique. If login is already exists in database registration will be terminated;
     * @param request Servlet request
     * @return Returns a string url of index page
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter("login");
        Connection connection = DBConnection.getInstance().getConnection();
        try{
            PreparedStatement statement = connection.prepareStatement(SQL_GET_LOGIN_FROM_FORM);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                logger.error("The user with" + login + "login already exists. Return to reg page");
                request.setAttribute("errorMessage", "User with this login already exists");
                page = PageURLConstant.REGISTRATION_PAGE;
                return page;
            } else {
                registrateNewManager(request);
                page = PageURLConstant.INDEX_PAGE;
                return page;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return page;
    }

    /**
     * This method will run if only user's wanted login is not already used to registration.
     * @param request Servlet request
     */
    private void registrateNewManager(HttpServletRequest request){
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        password = Cryptography.encryptPass(login, password);
        String name = defendeParams(request.getParameter("Name"));
        String surname = defendeParams(request.getParameter("Surname"));
        String patronymic = defendeParams(request.getParameter("Patronymic"));
        String telephone = defendeParams(request.getParameter("Telephone"));
        String email = defendeParams(request.getParameter("mail"));
        Manager manager = new Manager();
        manager.setMail(email);
        manager.setTelephone(telephone);
        manager.setManagerName(patronymic);
        manager.setManagerPatronymic(patronymic);
        manager.setManagerSurname(surname);
        manager.setManagerName(name);
        manager.setManagerLogin(login);
        manager.setManagerPassword(password);
        DAOManager daoManager = new DAOManager();
        daoManager.insertInformationInDatabase(manager);
        logger.info("New manager was registered!");
    }
}
