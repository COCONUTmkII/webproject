package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;

import javax.servlet.http.HttpServletRequest;

public class IndexCommand implements ActionCommand {
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.INDEX_PAGE;
        return page;
    }
}
