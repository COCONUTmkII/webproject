package by.home.chevrolet.command;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.encryption.Cryptography;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static by.home.chevrolet.statement.PrepareStatementConstant.GET_MANAGER_ID_BY_LOGIN;

public class AuthorizationCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(AuthorizationCommand.class);
    /**
     *
     * @param request
     * @return performs a command to authorize in a system.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter("login");
        String passwordFromJSP = request.getParameter("password");
        String dectyptedPass = Cryptography.decryptPass(login);
        Connection connection = DBConnection.getInstance().getConnection();
        if (dectyptedPass != null) {
            if (dectyptedPass.equals(passwordFromJSP)) {
                logger.info("Decrypted password equals user password");
                initSession(request, connection, login);
                page = PageURLConstant.MAIN_PAGE;
                return page;
            }
        }
        request.setAttribute("errorMessage", "Your login or password is wrong");
        logger.error("Login or password of user is wrong");
        page = PageURLConstant.INDEX_PAGE;
        return page;
    }

    /**
     *
     * @param request Get's the request
     * @param connection Get's the database connection
     * @param login Get's login from user
     */
        private void initSession (HttpServletRequest request, Connection connection, String login){
            HttpSession session = request.getSession();
            session.setAttribute("login", login);
            session.setAttribute("localeLanguage", "en");
            session.setAttribute("localeCountry", "US");
            PreparedStatement IDstatement = null;
            try {
                IDstatement = connection.prepareStatement(GET_MANAGER_ID_BY_LOGIN);
                IDstatement.setString(1, login);
                ResultSet resultSetInner = IDstatement.executeQuery();
                int idManager = 0;
                if (resultSetInner.next()) {
                    idManager = resultSetInner.getInt(1);
                }
                session.setAttribute("idManager", idManager);
                logger.info("initialized session to manager" + idManager);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
