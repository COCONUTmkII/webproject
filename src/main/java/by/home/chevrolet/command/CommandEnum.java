package by.home.chevrolet.command;

public enum CommandEnum {
    /**
     * This is enum that represents each command wanted by each manager
     */
    LOGIN{
        {this.command = new AuthorizationCommand();}
    },REGISTER_MANAGER{
        {this.command = new RegistrationCommand();}
    },REGISTRATION{
        {this.command = new SignUpCommand();}
    },LOGOUT{
        {this.command = new LogoutCommand();}
    },VEHICLE{
        {this.command = new VehicleCommand();}
    },OWNER{
        {this.command = new OwnerCommand();}
    },SALE{
        {this.command = new SaleCommand();}
    },COLLEAGUE{
        {this.command = new ColleagueCommand();}
    },RESERVE{
        {this.command = new CarReservationCommand();}
    },ADD_CAR{
        {this.command = new CarAdditionCommand();}
    },UPDATE_CAR{
        {this.command = new CarUpdateCommand();}
    },DEL_CAR{
        {this.command = new CarDeleteCommand();}
    },ADD_CLIENT{
        {this.command = new ClientAddCommand();}
    },UPDATE_CLIENT{
        {this.command = new ClientUpdateCommand();}
    },DEL_CLIENT{
        {this.command = new ClientDeleteCommand();}
    },RESERVE_CANCEL{
        {this.command = new ReserveCancelationCommand();}
    },SALE_UPDATE{
        {this.command = new SaleUpdateCommand();}
    },LANGUAGE{
        {this.command = new LanguageCommand();}
    },MAIN{
        {this.command = new MainPageCommand();}
    },INDEX{
        {this.command = new IndexCommand();}
    };
ActionCommand command;
    public ActionCommand getCommand(){
        return command;
    }
}
