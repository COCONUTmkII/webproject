package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOCar;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CarAdditionCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(CarAdditionCommand.class);
    /**
     * This command is used to add a new Car in CRM. If car is already exists,
     * system will do nothing.
     * @param request Servlet request
     * @return Returns a vehicle page if successful and index if manager is not authorized.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized. Returning to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        }
        String VIN = request.getParameter("VIN-add");
        String carModel = request.getParameter("model-add");
        String colour = request.getParameter("colour-add");
        String carPrice = request.getParameter("price-add");
        boolean availability = request.getParameter("availability-add") != null;
        boolean dataSheet = request.getParameter("data-sheet-add") != null;
        DAOCar daoCar = new DAOCar();
        daoCar.addNewCar(VIN, carModel, colour, carPrice, availability, dataSheet);
        page = PageURLConstant.VEHICLE_PAGE;
        logger.info("Car added into DB. Returning to vehicles page");
        return page;
    }
}
