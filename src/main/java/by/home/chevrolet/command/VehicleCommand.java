package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;

import javax.servlet.http.HttpServletRequest;

public class VehicleCommand implements ActionCommand {
    /**
     * This is a simple command that changes page to vehicles if manager wants
     * @param request Servlet request
     * @return Returns string url of vehicles page.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.VEHICLE_PAGE;
        return page;
    }
}
