package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;

import javax.servlet.http.HttpServletRequest;

public class SaleCommand implements ActionCommand {
    /**
     * This is a simple command to change page to sales if manager wants
     * @param request Servlet request
     * @return Returns a string url of sales page
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.SALE_PAGE;
        return page;
    }
}
