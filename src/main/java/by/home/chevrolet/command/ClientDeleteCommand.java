package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOClient;
import by.home.chevrolet.dao.DAOContract;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ClientDeleteCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(ClientDeleteCommand.class);
    /**
     * This command is used to delete client if it's necessary.
     * @param request Servlet request
     * @return Returns a owners page if command is successful. If manager is not authorized - to index.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        }
        int idClient = Integer.valueOf(request.getParameter("id-del"));
        DAOClient daoClient = new DAOClient();
        DAOContract daoContract = new DAOContract();
        daoClient.deleteClientIfNecessary(idClient);
        daoContract.deleteContractFromDBByClient(idClient);
        logger.warn("WARNING!! Manager" + idManager + "deleted a client with this id" + idClient);
        page = PageURLConstant.OWNER_PAGE;
        return page;
    }
}
