package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOContract;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class SaleUpdateCommand implements ActionCommand{
    private final static Logger logger = Logger.getLogger(SaleUpdateCommand.class);
    /**
     * This command is used to update information of each sale. E.g. to specify shipment date of a car
     * @param request Servlet request
     * @return Returns string sales url page or index if manager is not authorized.
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page =  null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Return to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        } else {
            int IDContract = Integer.parseInt(request.getParameter("id-upd"));
            String contractDate = request.getParameter("date-upd");
            String paymentDate = request.getParameter("payment-upd");
            String shipmentDate = request.getParameter("shipment-upd");
            DAOContract daoContract = new DAOContract();
            daoContract.updateContractStatus(IDContract, Date.valueOf(contractDate), Date.valueOf(paymentDate),
                    Date.valueOf(shipmentDate));
            page = PageURLConstant.SALE_PAGE;
            logger.info("Manager with id" + idManager + "updatet reserve information");
            return page;
        }
    }
}
