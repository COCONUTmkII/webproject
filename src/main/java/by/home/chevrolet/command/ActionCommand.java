package by.home.chevrolet.command;

import javax.servlet.http.HttpServletRequest;


@FunctionalInterface
public interface ActionCommand {

    /**
     *
     * @param request
     * @return Returns string that represents a page
     */
    public String executeCommand(HttpServletRequest request);
}
