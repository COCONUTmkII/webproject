package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOCar;
import by.home.chevrolet.dao.DAOContract;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

public class CarDeleteCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(CarDeleteCommand.class);
    /**
     * This command is used to delete a car from database if only this is necessary.
     * @param request Servlet request
     * @return Returns index page if manager is not authorized and vehicle page is command is successful
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized. Returning to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        }
        String VIN = request.getParameter("VIN-del");
        DAOCar daoCar = new DAOCar();
        DAOContract daoContract = new DAOContract();
        try {
            daoContract.deleteContractFromDBByCar(VIN);
            daoCar.deleteCarIfNecessary(VIN);
            page = PageURLConstant.VEHICLE_PAGE;
            logger.warn("WARNING!! Manager with id" + idManager + "deleted a car with" + VIN +"vin");
            return page;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return page;
    }
}
