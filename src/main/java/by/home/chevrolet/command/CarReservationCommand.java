package by.home.chevrolet.command;

import by.home.chevrolet.dao.DAOCar;
import by.home.chevrolet.dao.DAOClient;
import by.home.chevrolet.dao.DAOContract;
import by.home.chevrolet.entity.CarStatusEnum;
import by.home.chevrolet.entity.Contract;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class CarReservationCommand implements ActionCommand {
    private final static Logger logger = Logger.getLogger(CarReservationCommand.class);
    /**
     * This is the basic command to perform the main function of all managers.
     * This method reserves a car and drop it into the personal sales of each manager.
     * If car is successfully reserved it will change it's status to "reserved".
     * To reserve a car you need a verified client's telephone. If telephone is invalid you will
     * be dropped to error page in this case.
     * If your wanted car is already reserved you cannot reserve it anymore.
     * @param request Servlet request
     * @return Return a vehicles page if command successful. If manager is not authorized - to index
     *
     */
    @Override
    public String executeCommand(HttpServletRequest request) {

        String page = null;
        HttpSession session = request.getSession();
        Object idManager = session.getAttribute("idManager");
        if (idManager == null){
            logger.error("Manager is not authorized to perform this command. Returning to index");
            page = PageURLConstant.INDEX_PAGE;
            return page;
        } else {
            String contractDate = request.getParameter("contractDate");
            String paymentDate = request.getParameter("paymentDate");
            DAOCar daoCar = new DAOCar();
            DAOContract daoContract = new DAOContract();
            DAOClient daoClient = new DAOClient();
            int carID = daoCar.showCarIDByVIN(request.getParameter("VIN"));
            int idClient = daoClient.getClientIDByPhone(request.getParameter("clientPhone"));
            Contract contract = daoContract.getContractToCheckReserve(carID);
            if (idClient == 0){
                logger.error("Manager tryed to reserve a car to empty client. Redirect to error page");
                page = PageURLConstant.ERROR_PAGE;
                return page;
            } else if (contract.getId() == 0){
                daoContract.insertNewContract(carID, idClient, (Integer) idManager, Date.valueOf(contractDate), Date.valueOf(paymentDate));
                daoCar.changeCarStatusToReserved(carID, CarStatusEnum.RESERVED);
                page = PageURLConstant.VEHICLE_PAGE;
                logger.info("Car successfully reserved by" + idManager + "manager");
                return page;
            }
        }
        return page;
    }
}
