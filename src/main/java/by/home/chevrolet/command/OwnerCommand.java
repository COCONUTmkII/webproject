package by.home.chevrolet.command;

import by.home.chevrolet.url.PageURLConstant;

import javax.servlet.http.HttpServletRequest;

public class OwnerCommand implements ActionCommand {
    /**
     * This is a simple command to return manager to owners page if he wanted
     * @param request Servlet request
     * @return Returns string contains owners url
     */
    @Override
    public String executeCommand(HttpServletRequest request) {
        String page = PageURLConstant.OWNER_PAGE;
        return page;
    }
}
