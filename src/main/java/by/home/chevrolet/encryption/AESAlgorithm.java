package by.home.chevrolet.encryption;

import by.home.chevrolet.exception.BinFileNotFoundException;
import by.home.chevrolet.exception.DecryptionOfPasswordFailedException;
import org.apache.log4j.Logger;
import javax.crypto.Cipher;
import java.io.*;
import java.security.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class AESAlgorithm {
    private static final Logger logger = Logger.getLogger(AESAlgorithm.class);
    private final static long serialVersionUID = 1L;
    private static String fileName = null;

    /**
     * This is a method that creates a bin encrypted file that contain's a private and
     * public keys of each account that are also encrypted by RSA algorithm. Inserts a file in local system.
     * This file will be opened to get a keys for encrypt and decrypd a users password
     * @param login Login by which the file will be created.
     */
    public static void writeRSAKeys(String login){
        Map<String, Object> keys = null;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(512);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();
            keys =new HashMap<>();
            keys.put("private" + login, privateKey);
            keys.put("public" + login, publicKey);
            fileName = login + "encrypted.bin";
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName));
            objectOutputStream.writeObject(keys);
            objectOutputStream.close();
            logger.info("created a BIN encrypted file");
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
    }

    /**
     * This method gets an encrypted file and get private key from it to launch encryption of RSA algorithm.
     * This method require a login and password of a user. Login used to find a BIN file and password will be encrypted.
     * @param login Login of a user
     * @param password Password of a user
     * @return Returns encrypted password
     */
    public static String getHashEncryptedFile(String login, String password){
        Map<String, Object> readedHashMap = null;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName));
            readedHashMap = (Map<String, Object>) objectInputStream.readObject();
            PrivateKey privateKey = (PrivateKey)readedHashMap.get("private" + login);
            password = encryptMessage(password, privateKey);
            if (readedHashMap.isEmpty()){
                throw new BinFileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
        return password;
    }

    /**
     * This is a method that will decrypt user password by RSA algorithm. Gets encrypted hash and opens a bin file of
     * a user to decrypt his password. Login needed to get public key also for an decryption.
     * @param encryptedPassword Encrypted hash password of a user
     * @param login Login of a user
     * @return Returns decryptedPassword
     * @throws Exception Can throw exception
     */
    public static String decryptMessage(String encryptedPassword, String login) throws Exception{
        Cipher cipher = null;
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(login + "encrypted.bin"));
        Map<String, Object> readedEncrytedFile = (Map<String, Object>) inputStream.readObject();
        PublicKey publicKey = (PublicKey) readedEncrytedFile.get("public" + login);
        cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        String decryptedMessage = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedPassword)));
        if (decryptedMessage.isEmpty()){
            throw new DecryptionOfPasswordFailedException();
        }
        logger.info("decrypting password by algorithm");
        return decryptedMessage;
    }

    /**
     * This is a method that launches an RSA angorithm of encryption a password of a user. Gets a clean password and
     * private key and encrypts it
     * @param plainPassword Clean password of a user
     * @param privateKey Private key
     * @return Returns a string of encrypted password
     * @throws Exception Exception that can be thrown
     */
    private static String encryptMessage(String plainPassword, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        logger.info("encryptiong pass with alrorithm");
        return Base64.getEncoder().encodeToString(cipher.doFinal(plainPassword.getBytes()));
    }
}
