package by.home.chevrolet.encryption;

import org.apache.log4j.Logger;

public final class CSSDefendence {
    private static final Logger logger = Logger.getLogger(CSSDefendence.class);
    /**
     * This is a method that defence string parameters from cross site scripting.
     * @param parameterString String to be defended
     * @return Clean string of parameter
     */
    public static String defendeParams(String parameterString){
        logger.info("defending parameters from cross site scripting");
        parameterString = parameterString.replaceAll("<", " ");
        parameterString = parameterString.replaceAll(">", " ");
        return parameterString;
    }
}
