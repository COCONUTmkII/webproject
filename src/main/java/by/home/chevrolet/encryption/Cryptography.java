package by.home.chevrolet.encryption;

import by.home.chevrolet.db.DBConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.home.chevrolet.statement.PrepareStatementConstant.SQL_DECRYPT_PASS;

public class Cryptography {
    private final static Logger logger = Logger.getLogger(Cryptography.class);
    /**
     * This is a method that encrypt password of user. Gets BIN file and launches method from AES and
     * returns an encrypted password
     * @param loginFromForm Login of a user
     * @param passFromForm Clean password of a user
     * @return Return encrypted password
     */
    public static String encryptPass(String loginFromForm, String passFromForm) {
        String encryptedPass = null;
        try {
            AESAlgorithm.writeRSAKeys(loginFromForm);
            encryptedPass = AESAlgorithm.getHashEncryptedFile(loginFromForm, passFromForm);
            logger.info("encrypting password by password from form");
        } catch (Exception e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
        return encryptedPass;
    }

    /**
     * This is a method that decrypt a pass of a user by his login form with launching
     * method of decryption from AES.
     * @param loginFromForm Login from form
     * @return Returns clean password.
     */
    public static String  decryptPass(String loginFromForm) {
        String decryptedPass = null;
        try {
            Connection connection = DBConnection.getInstance().getConnection();
            String encryptedPass = null;
            PreparedStatement statement = connection.prepareStatement(SQL_DECRYPT_PASS);
            statement.setString(1, loginFromForm);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                encryptedPass = resultSet.getString(1);
            }
            decryptedPass = AESAlgorithm.decryptMessage(encryptedPass, loginFromForm);
            logger.info("decrypted pass by login from form");
        } catch (Exception e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
        return decryptedPass;
    }
}
