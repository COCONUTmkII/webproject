package by.home.chevrolet.controller;

import by.home.chevrolet.command.ActionCommand;
import by.home.chevrolet.command.ActionCommandFactory;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControllerServlet extends HttpServlet {
    private final static Logger logger = Logger.getLogger(ControllerServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * This is a controller that makes a processes according to managers commands.
     * @param request Servlet request
     * @param response Servlet response
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response){
        String page;
        ActionCommandFactory user = new ActionCommandFactory();
        ActionCommand command = user.defineCommandFromClient(request);
        try {
            page = command.executeCommand(request);
            if (page != null){
                logger.info("new command aquired" + command);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                dispatcher.forward(request, response);
            } else {
                logger.error("URL returned from command in empty. Redirect to error page");
                response.sendRedirect(PageURLConstant.ERROR_PAGE);
            }
        } catch (ServletException e) {
            logger.info(e.getCause());
        } catch (IOException e) {
            logger.error(e.getCause());
        }
    }
}
