package by.home.chevrolet.dao;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.entity.Manager;
import by.home.chevrolet.statement.PrepareStatementConstant;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static by.home.chevrolet.statement.PrepareStatementConstant.GET_ALL_COLLEAGUES;

public class DAOManager {
    private final static Logger logger = Logger.getLogger(DAOManager.class);

    /**
     * This is a method to create a list of managers that represent colleagues of a manager
     * @param idManager ID of a manager
     * @return List of manager collagues
     */
    public List<Manager> showColleagues(int idManager){
        Connection connection = DBConnection.getInstance().getConnection();
        List<Manager> colleagues = new ArrayList<>();
        try {
        PreparedStatement statement = connection.prepareStatement(GET_ALL_COLLEAGUES);
        statement.setInt(1, idManager);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            Manager manager = new Manager();
            manager.setManagerName(resultSet.getString("managerName"));
            manager.setManagerPatronymic(resultSet.getString("managerPatronymic"));
            manager.setManagerSurname(resultSet.getString("managerSurname"));
            manager.setTelephone(resultSet.getString("managerTelephone"));
            manager.setMail(resultSet.getString("mail"));
            colleagues.add(manager);
        }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }

        return  colleagues;
    }

    /**
     * This is a method that creates a new manager in database
     * @param manager This is a manager that will be created
     */
    public void insertInformationInDatabase(Manager manager) {
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            String mail = manager.getMail();
            String managerName = manager.getManagerName();
            String managerSurname = manager.getManagerSurname();
            String managerPatronymic = manager.getManagerPatronymic();
            String managerTelephone = manager.getTelephone();
            String managerLogin = manager.getManagerLogin();
            String managerPassword = manager.getManagerPassword();
            PreparedStatement statement = connection.prepareStatement(PrepareStatementConstant.SQL_CREATE_NEW_MANAGER);
            statement.setString(1, mail);
            statement.setString(2, managerName);
            statement.setString(3, managerSurname);
            statement.setString(4, managerPatronymic);
            statement.setString(5, managerTelephone);
            statement.setString(6, managerLogin);
            statement.setString(7, managerPassword);
            statement.execute();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
    }
}
