package by.home.chevrolet.dao;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.entity.ClientType;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static by.home.chevrolet.statement.PrepareStatementConstant.*;

public class DAOClientType {
    private final static Logger logger = Logger.getLogger(DAOClientType.class);
    /**
     * This is a method that shows client descryption - entity or individual by clients id
     * @param id ID of a client
     * @return returns a string representation of a client descryption
     */
    public String showDescription(int id) {
        Connection connection = DBConnection.getInstance().getConnection();
        String clientDescription = "";
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CLIENT_TYPE_DESCRIPTION);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                clientDescription = resultSet.getString("client_Description");
            }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return clientDescription;
    }

    /**
     * This method creates a list of client type.
     * @return List of client type
     */
    public List<ClientType> showAllInformation() {
        Connection connection = DBConnection.getInstance().getConnection();
        List<ClientType> clientTypes = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CLIENT_TYPE);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ClientType clientType = new ClientType();;
                clientType.setIdClientType(resultSet.getInt("idClientType"));
                clientType.setClientDescription(resultSet.getString("client_Description"));
                clientTypes.add(clientType);
            }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e){
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return clientTypes;
    }
}
