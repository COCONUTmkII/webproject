package by.home.chevrolet.dao;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.entity.Client;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static by.home.chevrolet.statement.PrepareStatementConstant.*;

public class DAOClient{
    private final static Logger logger = Logger.getLogger(DAOClient.class);
    /**
     * This method returns the ID of a client accordint to clients telephone. Telephone of a client must be valid.
     * @param clientTelephone Client's telephone.
     * @return Returns ID of a client
     */
    public int getClientIDByPhone(String clientTelephone){
        Connection connection = DBConnection.getInstance().getConnection();
        int clientID = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CLIENT_ID_BY_PHONE);
            statement.setString(1, clientTelephone);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                clientID = resultSet.getInt("idClient");
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return clientID;
    }

    /**
     * This method creates a list of all clients to show them in owners page.
     * @return Returns a list of a clients
     */
    public List<Client> showAllInformation() {
        Connection connection = DBConnection.getInstance().getConnection();
        List<Client> clients = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ALL_CLIENTS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Client client = new Client();
                client.setIdClient(resultSet.getInt("idClient"));
                client.setClientName(resultSet.getString("clientName"));
                client.setClientPatronymic(resultSet.getString("clientPatronymic"));
                client.setClientSurname(resultSet.getString("clientSurname"));
                client.setClientMail(resultSet.getString("clientMail"));
                client.setTelephone(resultSet.getString("telephone"));
                client.setNotes(resultSet.getString("notes"));
                client.setIdType(resultSet.getInt("idClientType"));
                clients.add(client);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e){
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return clients;
    }

    /**
     * This method is used to show client information according to contract(sale) id to be
     * presented in sales
     * @param idContract ID of a contract
     * @return Return a client
     */
    public Client showClientInformation(int idContract){
        Connection connection = DBConnection.getInstance().getConnection();
        Client client = new Client();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CLIENT_INFORMATION);
            statement.setInt(1, idContract);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                client.setClientName(resultSet.getString("clientName"));
                client.setClientSurname(resultSet.getString("clientSurname"));
                client.setTelephone(resultSet.getString("telephone"));
                client.setClientMail(resultSet.getString("clientMail"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.error(e.getCause());
            }
        }
        return client;
    }

    /**
     * This method is used to create new client in a database. Can throw SQL Exception
     * @param email Email of a new client
     * @param telephone Telephone of a new client. Must be unique.
     * @param name Name of a client
     * @param surname Surname of a client
     * @param patronymic Patronymic of a client
     * @param notes Extra notes of a client. Can be empty. This field is not required.
     * @param clientType Type of a client. Can be individual or entity.
     * @throws SQLException SQL Exception that can be throws
     */
    public void addNewClient(String email, String telephone, String name, String surname,
                             String patronymic, String notes, Integer clientType) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(ADD_NEW_CLIENT);
            statement.setString(1, email);
            statement.setString(2, telephone);
            statement.setString(3, notes);
            statement.setString(4, name);
            statement.setString(5, surname);
            statement.setString(6, patronymic);
            statement.setInt(7, clientType);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            connection.close();
        }
    }

    /**
     * This method is used to delete client from database if its necessary.
     * @param ID ID of a client that will be deleted
     */
    public void deleteClientIfNecessary(int ID){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_CLIENT_IF_NECESSARY);
            statement.setInt(1, ID);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
    }


    /**
     * This method is used to update client info in database. Can throw SQL exception
     * @param ID ID of a client that will be updated
     * @param mail Client updated mail.
     * @param telephone Client updated telephone.
     * @param notes Client updated notes. Can be empty
     * @param name Client updated name
     * @param surname Client updated surname
     * @param patronymic Client updated patronymic
     * @throws SQLException SQL Exception that can be thrown
     */
    public void updateClient(int ID, String mail, String telephone, String notes, String name, String surname,
                                String patronymic) throws SQLException {

        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_CLIENT_INFO);
            statement.setString(1, mail);
            statement.setString(2, telephone);
            statement.setString(3, notes);
            statement.setString(4, name);
            statement.setString(5, surname);
            statement.setString(6, patronymic);
            statement.setInt(7, ID);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            connection.close();
        }
    }
}
