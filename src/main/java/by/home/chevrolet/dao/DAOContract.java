package by.home.chevrolet.dao;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.entity.CarStatusEnum;
import by.home.chevrolet.entity.Contract;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static by.home.chevrolet.statement.PrepareStatementConstant.*;

public class DAOContract{
    private final static Logger logger = Logger.getLogger(DAOContract.class);

    /**
     * This method is used to get contract to check does car reserved.
     * If contract exists with particular car id that's required, than this car is already reserved.
     * @param idCar ID of a car to be checked.
     * @return Return a contract.
     */
    public Contract getContractToCheckReserve(int idCar){
        Connection connection = DBConnection.getInstance().getConnection();
        Contract contract = new Contract();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ONE_CONTRACT_TO_RESERVE);
            statement.setInt(1, idCar);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                contract.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return contract;
    }

    /**
     * This method is used to show a manager his personal sales(contracts) accordint to manager's ID
     * @param idManager ID of a manager.
     * @return Returns a list of all contracts of a manager.
     */
    public List<Contract> showMySales(int idManager){
        Connection connection = DBConnection.getInstance().getConnection();
        List<Contract> contracts = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ALL_MY_CONTRACTS);
            statement.setInt(1, idManager);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Contract contract = new Contract();
                contract.setContractDate(resultSet.getDate("contractDate"));
                contract.setPaymentDate(resultSet.getDate("paymentDate"));
                contract.setShipmentDate(resultSet.getDate("shipmentDate"));
                contract.setCarID(resultSet.getInt("idCar"));
                contract.setClientID(resultSet.getInt("idClient"));
                contract.setManagerID(resultSet.getInt("idManager"));
                contract.setId(resultSet.getInt("id"));
                contracts.add(contract);
            }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return contracts;
    }

    /**
     * This method is used to create a new contract. Contract will be created if only contract is not exists.
     * @param idCar ID of a car that will be reserved.
     * @param idClient ID of a client who will own a car.
     * @param idManager ID of a manager that create a contract (reserve a car)
     * @param contractDate Contract date
     * @param paymentDate Payment pate of a client
     */
    public void insertNewContract(int idCar, int idClient, int idManager, Date contractDate, Date paymentDate){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_NEW_CONTRACT);
            statement.setInt(1, idCar);
            statement.setInt(2, idClient);
            statement.setInt(3, idManager);
            statement.setDate(4, (java.sql.Date) contractDate);
            statement.setDate(5, (java.sql.Date) paymentDate);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * This method is delete a contract(reserve) from database by client ID.
     * This method is performed when manager is deleting client from a database.
     * @param idClient ID of a client
     */
    public void deleteContractFromDBByClient(int idClient){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            DAOClient daoClient = new DAOClient();
            PreparedStatement statement = connection.prepareStatement(DELETE_CONTRACT_FROM_DB_BY_CLIENT);
            statement.setInt(1, idClient);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * This method is used to update a contract(reserve) status.
     * @param IDContract ID of a contract
     * @param contractDate Updated contract date
     * @param paymentDate Updated payment date
     * @param shipmentDate Shipment date
     */
    public void updateContractStatus(int IDContract, Date contractDate, Date paymentDate, Date shipmentDate){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_CONTRACT_STATUS);
            statement.setDate(1, (java.sql.Date) contractDate);
            statement.setDate(2, (java.sql.Date) paymentDate);
            statement.setDate(3, (java.sql.Date) shipmentDate);
            statement.setInt(4, IDContract);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * This is a method to safe delete a contract from managers sales (or 'cancel a contract')
     * @param VIN VIN of a car that need's to be canceled from reservation.
     * @param ID ID of a contract
     */
    public void deleteContractFromSales(String VIN, int ID){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statementOfStatus = connection.prepareStatement(UPDATE_CAR_STATUS_FROM_SALES);
            statementOfStatus.setInt(1, CarStatusEnum.NOT_RESERVED.ordinal());
            statementOfStatus.setString(2, VIN);
            statementOfStatus.executeUpdate();
            PreparedStatement statementOfDelete = connection.prepareStatement(DELETE_CONTRACT_FROM_SALES);
            statementOfDelete.setInt(1, ID);
            statementOfDelete.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * This is a method that deletes a contract from database if car was deleted in database.
     * @param VIN VIN number of a car
     */
    public void deleteContractFromDBByCar(String VIN){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            Integer idCar = 0;
            PreparedStatement statementForIdCar = connection.prepareStatement(GET_CAR_ID_BY_VIN);
            statementForIdCar.setString(1, VIN);
            ResultSet resultSet = statementForIdCar.executeQuery();
            if (resultSet.next()){
              idCar = resultSet.getInt("idCar");
            }
            PreparedStatement statement = connection.prepareStatement(DELETE_CONTRACT_FROM_DB_BY_CAR);
            statement.setInt(1, idCar);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
    }
}
