package by.home.chevrolet.dao;

import by.home.chevrolet.db.DBConnection;
import by.home.chevrolet.entity.Car;
import by.home.chevrolet.entity.CarStatusEnum;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import static by.home.chevrolet.statement.PrepareStatementConstant.*;

public class DAOCar {
    private final static Logger logger = Logger.getLogger(DAOCar.class);

    /**
     * This method is used to show a car information - car's VIN and car's model according to a
     * id of each contract, that creates if manager will reserve a car.
     * @param idContract ID of each contract(reserve)
     * @return Returns a car object that contains vin and model.
     */
    public Car showCarInformation(int idContract){
        Connection connection = DBConnection.getInstance().getConnection();
        Car car = new Car();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CAR_INFORMATION);
            statement.setInt(1, idContract);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                car.setVIN(resultSet.getString("VIN"));
                car.setCarModel(resultSet.getString("carModel"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return car;
    }

    /**
     * This method is used to create a list of cars to show in vehicles page.
     * @return Returnes a list of cars
     */
    public List<Car> showAllInformation() {
        List<Car> result = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_CAR);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Car car = new Car();
                car.setIdCar(resultSet.getInt("idCar"));
                car.setVIN(resultSet.getString("VIN"));
                car.setCarModel(resultSet.getString("carModel"));
                car.setColour(resultSet.getString("Colour"));
                car.setCarPrice(resultSet.getInt("carPrice"));
                car.setAvailability(resultSet.getBoolean("availability"));
                car.setRecyclingCollection(resultSet.getBoolean("recyclingCollection"));
                car.setStatus(resultSet.getBoolean("status"));
                result.add(car);
            }
            logger.info("cars added to show information");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getCause());
                logger.error(e.getMessage());
            }
        }
        return result;
    }

    /**
     * This is a method that change car status to "reserved" if manager will reserve a car.
     * @param carID Car ID
     * @param carStatusEnum Parameter that contains two statuses - reserved or not reserved
     */
    public void changeCarStatusToReserved(int carID, CarStatusEnum carStatusEnum){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CAR_STATUS);
            preparedStatement.setInt(1, CarStatusEnum.RESERVED.ordinal());
            preparedStatement.setInt(2, carID);
            preparedStatement.execute();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            logger.error(e.getCause());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.error(e.getCause());
            }
        }
    }

    /**
     * This method is used to get ID of a car by VIN-number.
     * @param VIN VIN number of a car
     * @return Return ID of a car
     */
    public int showCarIDByVIN(String VIN){
        int carID = 0;
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CAR_ID_BY_VIN);
            statement.setString(1, VIN);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                carID = resultSet.getInt("idCar");
            }
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.error(e.getCause());
            }
        }
        return carID;
    }

    /**
     * This method is used to add a new car in database.
     * @param VIN Vin number of a new car. This parameter must be unique
     * @param carModel Model of a car
     * @param colour Colour of a car
     * @param carPrice Price of a car
     * @param availabiity Does car available in warehouse
     * @param recuclingCollection Does car contains a recycle collection.
     */
    public void addNewCar(String VIN, String carModel, String colour, String carPrice,
                          boolean availabiity, boolean recuclingCollection){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(ADD_NEW_CAR);
            statement.setString(1, VIN);
            statement.setString(2, carModel);
            statement.setString(3, colour);
            statement.setString(4, carPrice);
            statement.setBoolean(5, availabiity);
            statement.setBoolean(6, recuclingCollection);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.error(e.getCause());
            }
        }
    }

    /**
     * This method is used to update some car information.
     * @param price Changed price of a car
     * @param VIN VIN number of a car that wants to be changed.
     * @param availability Changed availability
     * @param recyclingCollection Chenged recycle collection availability.
     */
    public void updateInformationInCar(int price, String VIN, boolean availability, boolean recyclingCollection){
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_CAR_INFO);
            statement.setInt(1, price);
            statement.setBoolean(2, availability);
            statement.setBoolean(3, recyclingCollection);
            statement.setString(4, VIN);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
                logger.error(e.getCause());
            }
        }
    }

    /**
     * This method deletes a car from a database according to VIN. Can throw SQL Exception.
     * @param VIN VIN number of a car that's need tp be deleted.
     * @throws SQLException Exception that can be thrown.
     */
    public void deleteCarIfNecessary(String VIN) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_CAR_IF_NECESSARY);
            statement.setString(1, VIN);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } finally {
            connection.close();
        }
    }
}
