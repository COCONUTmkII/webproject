package by.home.chevrolet.filter;
import by.home.chevrolet.command.CommandEnum;
import by.home.chevrolet.url.PageURLConstant;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CommandFilter implements Filter {
    private static final Logger logger = Logger.getLogger(CommandFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * This is a command filter that controls the commands to be delegated to controller
     * @param servletRequest Request from servlet
     * @param servletResponse   Response from servlet
     * @param filterChain chain of filter responsobility
     * @throws IOException IOException that can be thrown
     * @throws ServletException ServletException that can be thrown
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String command = request.getParameter("command");
            if ((command.equals("login") || (command.equals("registration")))) {
                filterChain.doFilter(request, response);
            }  else {
                HttpSession session = request.getSession();
                Object idManager = session.getServletContext().getAttribute("idManager");
                if (idManager == null){
                    logger.error("Manager id is null. Thus, he's not authorized. Redirect to index");
                    response.sendRedirect(request.getContextPath() + PageURLConstant.INDEX_PAGE);
                } else {
                    filterChain.doFilter(request, response);
                }
            }
        }

    @Override
    public void destroy() {

    }
}
