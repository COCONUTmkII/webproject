package by.home.chevrolet.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LocaleFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Filter that sets UTF-8 format to pages
     * @param servletRequest Servlet request
     * @param servletResponse Servlet response
     * @param filterChain filter chain of responsobility
     * @throws IOException IOException that can be thrown
     * @throws ServletException Servlet Exception that can be thrown
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        filterChain.doFilter(request, response);

    }
    @Override
    public void destroy() {

    }
}
