package by.home.chevrolet.entity;

public class Car {
    private int idCar;
    private String VIN;
    private String carModel;
    private String colour;
    private int carPrice;
    private boolean availability;
    private boolean recyclingCollection;
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(int carPrice) {
        this.carPrice = carPrice;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public boolean isRecyclingCollection() {
        return recyclingCollection;
    }

    public void setRecyclingCollection(boolean recyclingCollection) {
        this.recyclingCollection = recyclingCollection;
    }
}
