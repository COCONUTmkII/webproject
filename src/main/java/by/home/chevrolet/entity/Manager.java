package by.home.chevrolet.entity;

public class Manager {
    private int idManager;
    private String mail;
    private String managerName;
    private String managerSurname;
    private String managerPatronymic;
    private String telephone;
    private String managerLogin;
    private String managerPassword;

    public Manager(){}

    public Manager(String managerLogin, String managerPassword, String mail,  String telephone,
                   String managerName, String managerSurname, String managerPatronymic) {
        this.mail = mail;
        this.managerName = managerName;
        this.managerSurname = managerSurname;
        this.managerPatronymic = managerPatronymic;
        this.telephone = telephone;
        this.managerLogin = managerLogin;
        this.managerPassword = managerPassword;
    }

    public int getIdManager() {
        return idManager;
    }

    public void setIdManager(int idManager) {
        this.idManager = idManager;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerSurname() {
        return managerSurname;
    }

    public void setManagerSurname(String managerSurname) {
        this.managerSurname = managerSurname;
    }

    public String getManagerPatronymic() {
        return managerPatronymic;
    }

    public void setManagerPatronymic(String managerPatronymic) {
        this.managerPatronymic = managerPatronymic;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getManagerLogin() {
        return managerLogin;
    }

    public void setManagerLogin(String managerLogin) {
        this.managerLogin = managerLogin;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "mail='" + mail + '\'' +
                ", managerName='" + managerName + '\'' +
                ", managerSurname='" + managerSurname + '\'' +
                ", managerPatronymic='" + managerPatronymic + '\'' +
                ", telephone='" + telephone + '\'' +
                ", managerLogin='" + managerLogin + '\'' +
                ", managerPassword='" + managerPassword + '\'' +
                '}';
    }
}
