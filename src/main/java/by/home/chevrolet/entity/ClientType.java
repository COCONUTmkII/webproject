package by.home.chevrolet.entity;

public class ClientType {
    private int idClientType;
    private String clientDescription;

    public int getIdClientType() {
        return idClientType;
    }

    public void setIdClientType(int idClientType) {
        this.idClientType = idClientType;
    }

    public String getClientDescription() {
        return clientDescription;
    }

    public void setClientDescription(String clientDescription) {
        this.clientDescription = clientDescription;
    }
}
