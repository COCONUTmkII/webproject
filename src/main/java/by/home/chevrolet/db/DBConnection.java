package by.home.chevrolet.db;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConnection {
    private final static Logger logger = Logger.getLogger(DBConnection.class);

    private DBConnection(){
    }

    private static DBConnection instance = null;

    public static DBConnection getInstance(){
        if (instance==null)
            instance = new DBConnection();
        return instance;
    }

    public Connection getConnection(){
        Context ctx;
        Connection c = null;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/mainPool");
            c = ds.getConnection();
        } catch (NamingException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        } catch (SQLException e) {
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
        return c;
    }
}