package by.home.chevrolet.locale;

import org.apache.log4j.Logger;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleTag extends TagSupport {
    private static final Logger logger = Logger.getLogger(LocaleTag.class);

    private String text;

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }

    /**
     * This is a custom tag that changes language according to locale
     * @return Return int that skips the body evaluation. Must be returned by do start tag
     */
    @Override
    public int doStartTag() {
        JspWriter out = pageContext.getOut();
        String language = String.valueOf(pageContext.getSession().getAttribute("localeLanguage"));
        String country = String.valueOf(pageContext.getSession().getAttribute("localeCountry"));
        Locale current = new Locale(language, country);
        ResourceBundle resourceBundle = ResourceBundle.getBundle("property", current);
        try {
            out.write(resourceBundle.getString(text));
        } catch (IOException e){
            logger.error(e.getCause());
            logger.error(e.getMessage());
        }
        return SKIP_BODY;
    }
}
