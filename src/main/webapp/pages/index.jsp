<%--
  Created by IntelliJ IDEA.
  User: COCONUTmkII
  Date: 18.07.2019
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Chevrolet CRM Authorisation</title>
    <link rel="stylesheet" href="../css/login.css"/>
  </head>
<body>
  <form class="box" action="/controller"  method="POST" name="vform">
  <h1>
    <img src= "../images/chevrolet_logo.png">
  </h1>
      <c:set var ="error" value="${requestScope.errorMessage}"/>
      <c:if test="${error.length() > 0}">
          <span style="color: red">${error}</span>
      </c:if>
  <input type="hidden" name="command" value="login">
  <input type="text" name="login"  placeholder="Username" required>
  <input type="password" name="password" placeholder="Password" required>
  <input type="submit" name="" id="authorization" value="Login">
  </form>
  <form class = "registration" action = "/controller" method = "post">
    <input type="hidden" name="command" value="registration">
    <input type = "submit" value = "Sign Up">
  </form>

</body>
  <script type="text/javascript" src = "../js/authorization.js"></script>
</html>
