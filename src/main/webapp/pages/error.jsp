<%--
  Created by IntelliJ IDEA.
  User: COCONUTmkII
  Date: 12.08.2019
  Time: 0:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
</head>
<link href="https://fonts.googleapis.com/css?family=Encode+Sans+Semi+Condensed:100,200,300,400" rel="stylesheet">
<link rel="stylesheet" href="/css/error_page_style.css">
<body class="loading">
<h1>500</h1>
<h2>Unexpected Error <b>:(</b></h2>
<h3><form action="/controller" method="post">
    <button type="submit" class = "main_page_link">
    <input type="hidden" name="command" value="index">
    BACK TO AUTHORIZATION
</button></form>
</h3>
<div class="gears">
    <div class="gear one">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
    <div class="gear two">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
    <div class="gear three">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="/js/error.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>
